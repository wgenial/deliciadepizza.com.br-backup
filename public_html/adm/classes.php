<?php
if(file_exists("../includes/conexao.php")){
	require_once("../includes/conexao.php");
}else{
	echo "não existe: ../includes/conexao.php";
}

class Login {

	public $login;
	public $ativo;
	public $adm;
	
   function set($prop,$value){
      $this->$prop = $value;
   }
	
	function existe($total){
	
		// Caso o usuário tenha digitado um login válido o número de linhas será 1..
		
		if($total){
			$_SESSION["login"] = $this->login;
			$_SESSION["adm"] =  $this->adm;
			$_SESSION["ativo"] =  $this->ativo;
			$_SESSION["nome"] =  $this->nome;
			
		  echo "login: ".$_SESSION["login"];
			
			echo "<script>window.location = 'inicio.php'</script>";
			exit;
		}
		else{
			echo "<script>alert('Login ou senha inválida'); window.location='index.php'</script>";
		    echo "<input type='button' class='botao' value='Voltar' onClick=window.location='index.php' />";
		}
	}//function existe

}//class login

class ResumoNoticias {

	private $id;
	private $titulo;
	private $texto;
	private $thumb;
	
	function set($prop,$value){
	  $this->$prop = $value;
	}
	
	function thumb($id){
		if(file_exists("noticias/".$id.".gif")){
			$thumb = "noticias/".$id.".gif";
		}
		if(file_exists("noticias/".$id.".png")){
			$thumb = "noticias/".$id.".png";
		}
		if(file_exists("noticias/".$id.".jpg")){
			$thumb = "noticias/".$id.".jpg";
		}
		return $thumb;
	}
	function titulo($titulo){
		$titulo = strip_tags($titulo);
		
		return $titulo;
	}
	function texto($texto, $tamanho){
		$texto = strip_tags($texto);
		$texto = substr($texto, 0, $tamanho)."...";
		return $texto;
	}

}//class resumo noticia

class Logado {
	
   private $login;
   private $ativo;
	
   function set($prop,$value){
      $this->$prop = $value;
   }
	
	function Sessao($login, $ativo){
        if(strlen($login) < 3){
            echo "<script>alert('Area restrita. Informe Login e Senha.')</script>";
            echo "<script>window.location = 'index.php'</script>";
		}
        if($ativo == "N"){
            echo "<script>alert('Usuario Temporariamente Suspenso - Consulte seu supervisor.')</script>";
            echo "<script>window.location = 'index.php'</script>";
		}
	}//function sessao
	
} // class logado

class Administrador {
	
   private $adm;
	
	function Sessao($adm){
        if($adm != "S"){
            echo "<script>alert('Area restrita para administradores.')</script>";
            echo "<script>window.location = 'inicio.php'</script>";
		}
	}//function sessao
	
	
} // class logado



function converteData($Data){
	 if (strstr($Data, "/"))//verifica se tem a barra /
	 {
	   $d = explode ("/", $Data);//tira a barra
	   $rstData = "$d[2]-$d[1]-$d[0]";//separa as datas $d[2] = ano $d[1] = mes etc...
	   return $rstData;
	} elseif(strstr($Data, "-")){
	  $d = explode ("-", $Data);
	  $rstData = "$d[2]/$d[1]/$d[0]";
	  return $rstData;
	 }else{
	   return "Data invalida";
	 }
 }
 function msgBox($tipo, $mensagem){
		echo "<script type='text/javascript'>alert('".$mensagem."'); history.go(-1);</script>";
		exit;
 }
 function checado($valor1, $valor2){
 	if(utf8_encode($valor1) == utf8_encode($valor2)){
		echo "checked";
	}
 }
 function selecionado($valor1, $valor2){
 	if($valor1 == $valor2){
		return "selected='true'";
	}
 }
function checkPermissao($opcao, $quant, $string){

	$per = explode("|", $string);
	
	for($i=0;$i<$quant+2;$i++){
		if($opcao == $per[$i]){
			echo "checked";
		}
	}
}
function imagemTipo($endereco){
	if(file_exists($endereco.".gif")){
		$tipo = $endereco.".gif";
	}
	if(file_exists($endereco.".png")){
		$tipo = $endereco.".png";
	}
	if(file_exists($endereco.".jpg")){
		$tipo = $endereco.".jpg";
	}
	return $tipo;
}
function mes($m){
	switch($m){
		case "01":
			echo "JAN";
			break;
		case "02":
			echo "FEV";
			break;
		case "03":
			echo "MAR";
			break;
		case "04":
			echo "ABR";
			break;
		case "05":
			echo "MAI";
			break;
		case "06":
			echo "JUN";
			break;
		case "07":
			echo "JUL";
			break;
		case "08":
			echo "AGO";
			break;
		case "09":
			echo "SET";
			break;
		case "10":
			echo "OUT";
			break;
		case "11":
			echo "NOV";
			break;
		case "12":
			echo "DEZ";
			break;
	}
}

function removeAcentos($str, $enc = 'UTF-8'){
 
	$acentos = array(
			'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
			'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
			'C' => '/&Ccedil;/',
			'c' => '/&ccedil;/',
			'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
			'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
			'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
			'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
			'N' => '/&Ntilde;/',
			'n' => '/&ntilde;/',
			'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
			'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
			'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
			'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
			'Y' => '/&Yacute;/',
			'y' => '/&yacute;|&yuml;/',
			'a.' => '/&ordf;/',
			'o.' => '/&ordm;/'
	);
	
	return preg_replace($acentos, array_keys($acentos), htmlentities($str,ENT_NOQUOTES, $enc));
}

function criarOptions($res, $valor, $valor2, $nome){
	while($rs = mysql_fetch_array($res)){
		echo "<option value='".$rs[$valor]."' ".selecionado($rs[$valor], $valor2)."> \r\n";
			echo $rs[$nome]."\r\n";
		echo "</option>";
	}
}

function obrigatorio($string, $campo){
	if($string == ""){
		echo "<script>alert('Campo obrigatório - $campo')</script>";
	}
}
function apagaArquivo($endereco){
	if(file_exists($endereco)){
		unlink($endereco);
	}
}


?>
