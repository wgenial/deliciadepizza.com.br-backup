<?php
require_once("model/listaBanners.class.php");

$tamanhoPagina = 200;

$pagina = isset($_GET["pagina"]) ? mysql_real_escape_string($_GET["pagina"]) : 0;
$inicioPagina = (($pagina * $tamanhoPagina) - $tamanhoPagina);
if($inicioPagina < 0){
	$inicioPagina = 0;
}

$h1 = "Banners";
$pasta  = "banners";

$lista = new Banner;

$lista->set("paginacao", $inicioPagina.", ".$tamanhoPagina);

$lista->consultarBanners();
?>