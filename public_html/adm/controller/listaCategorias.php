<?php
require_once("model/listaCategorias.class.php");

$tamanhoPagina = 200;

$pagina = isset($_GET["pagina"]) ? mysql_real_escape_string($_GET["pagina"]) : 0;
$inicioPagina = (($pagina * $tamanhoPagina) - $tamanhoPagina);
if($inicioPagina < 0){
	$inicioPagina = 0;
}

$h1 = "Categorias";
$pasta  = "categorias";

$lista = new Categoria;

$lista->set("paginacao", $inicioPagina.", ".$tamanhoPagina);

$lista->consultarCategorias();
?>