<?php
require_once("model/listaNoticias.class.php");

$tamanhoPagina = 5;

$pagina = isset($_GET["pagina"]) ? mysql_real_escape_string($_GET["pagina"]) : 0;
$inicioPagina = (($pagina * $tamanhoPagina) - $tamanhoPagina);
if($inicioPagina < 0){
	$inicioPagina = 0;
}

$h1 = "Notícias";
$pasta  = "noticias";

$lista = new Noticia;

$lista->set("paginacao", $inicioPagina.", ".$tamanhoPagina);

$lista->consultarNoticias();
?>