<?php
require_once("model/produtos.class.php");

$nome = $_SESSION["nome"];
$acesso = $_SERVER['REQUEST_URI'];

//Eu quero criar um controle para que o endereço que enviou a informação seja checado.
if($acesso != "/noticias_produtos.php"){
    //echo $acesso;
}

$pasta = "../produtos/";
$tabela = PREFIXO."produtos";

if(isset($_POST["id"]) and $_POST["id"] != ""){
    $id = $_POST["id"];
    $tipo = "update";
}else{
    $id = "";
    $tipo = "insert";
}

$categoria = isset($_POST["categoria"]) ? $_POST["categoria"] : "";
$titulo = isset($_POST["titulo"]) ? $_POST["titulo"] : "";
$texto = isset($_POST["texto"]) ? $_POST["texto"] : "";
$preco = isset($_POST["preco"]) ? $_POST["preco"] : "";

obrigatorio($titulo, "Título");
obrigatorio($texto, "Texto");
obrigatorio($categoria, "Categoria");

$produto = new Produtos;

$produto->set("categoria", $categoria);
$produto->set("titulo", $titulo);
$produto->set("texto", $texto);
$produto->set("preco", $preco);
$produto->set("tipo", $tipo);
$produto->set("pasta", $pasta);
$produto->set("tabela", $tabela);
$produto->set("id", $id);


if($produto->gravaDados() == "ok"){
    echo "<span style='color:green'>Dados Gravados com sucesso.</span>";
    exit;
}else{
	echo "<span style='color:red'>Não foi possível gravar essas informações</span>";
	exit;
}
?>