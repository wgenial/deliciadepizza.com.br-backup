﻿<?php session_start();

require_once("config.php");
require_once("classes.php");

if (isset($_GET['rota'])) {
    include($_GET['rota'].".tpl");
	$log = new Logado;
	$log->set('login', $_SESSION["login"]);
	$log->Sessao($_SESSION["login"], $_SESSION["ativo"]);

	$nome = $_SESSION["nome"];
	
} else {
    include('view/login.tpl');
}
?>