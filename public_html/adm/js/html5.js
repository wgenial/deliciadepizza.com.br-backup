// For discussion and comments, see: http://remysharp.com/2009/01/07/html5-enabling-script/
(function(){if(!/*@cc_on!@*/0)return;var e = "abbr,article,aside,audio,bb,canvas,datagrid,datalist,details,dialog,eventsource,figure,footer,header,hgroup,mark,menu,meter,nav,output,progress,section,time,video".split(','),i=e.length;while(i--){document.createElement(e[i])}})()

$(function() {
	   if(!$.support.placeholder) {
			   var active = document.activeElement;
			   $(':text').focus(function () {
					   if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
							   $(this).val('').removeClass('hasPlaceholder');
					   }
			   }).blur(function () {
					   if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
							   $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
					   }
			   });
			   $(':text').blur();
				$('textarea').focus(function () {
					   if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
							   $(this).val('').removeClass('hasPlaceholder');
					   }
			   }).blur(function () {
					   if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
							   $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
					   }
			   });
			   $('textarea').blur();

			   $(active).focus();
			   $('form').submit(function () {
					   $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
			   });
	   }
});
$(function() {
	   if(!$.support.placeholder) {
			   var active = document.activeElement;
			   $('#email').focus(function () {
					   if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
							   $(this).val('').removeClass('hasPlaceholder');
					   }
			   }).blur(function () {
					   if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
							   $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
					   }
			   });
			   $('#email').blur();
				$('textarea').focus(function () {
					   if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
							   $(this).val('').removeClass('hasPlaceholder');
					   }
			   }).blur(function () {
					   if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
							   $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
					   }
			   });
			   $('textarea').blur();

			   $(active).focus();
			   $('form').submit(function () {
					   $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
			   });
	   }
});
$(function() {
	   if(!$.support.placeholder) {
			   var active = document.activeElement;
			   $('#telefone').focus(function () {
					   if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
							   $(this).val('').removeClass('hasPlaceholder');
					   }
			   }).blur(function () {
					   if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
							   $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
					   }
			   });
			   $('#telefone').blur();
				$('textarea').focus(function () {
					   if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
							   $(this).val('').removeClass('hasPlaceholder');
					   }
			   }).blur(function () {
					   if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
							   $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
					   }
			   });
			   $('textarea').blur();

			   $(active).focus();
			   $('form').submit(function () {
					   $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
			   });
	   }
});