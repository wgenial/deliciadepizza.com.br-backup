<?php
require_once("../includes/conexao.php");

class Produtos {

	private $tipo;
	private $tabela;
	private $texto;
	private $link;
	private $categoria;
	private $id;
	private $pasta;
	private $proximo;

	function set($nome, $valor){
		$this->$nome = $valor;
	}

	function dadosImagens(){
		$obj = new Conexao;

		//As medidas das imagens estão na tabela de categorias
		$sql = "select * from ".PREFIXO."categorias_banners where id = ".$this->categoria;

		$obj->set('sql', $sql);
		$res = $obj->query();
		$rs = mysql_fetch_array($res);

		$this->largura_thumb = $rs["largura"];
		$this->altura_thumb = $rs["altura"];

		//Pegando o id
		$sql = "select * from ".PREFIXO."banners where link = '".$this->link ."' order by id desc limit 1";

		$obj->set('sql', $sql);
		$res = $obj->query();
		$rs = mysql_fetch_array($res);

		$this->id = $rs["id"];

	}

	function gravaImagens(){

		$this->dadosImagens();
		$this->erro = "";

		//Só recebemos 20 imagens por envio de formulário. Não queremos uma conexão muito pesada, não é?
		if(isset($_FILES['imagem']['tmp_name']) and substr($_FILES['imagem']['name'], -3) == "jpg"){
		    if(move_uploaded_file($_FILES['imagem']['tmp_name'], $this->pasta."/".$this->id. ".jpg")){

		        $this->filename = $this->pasta.$this->id. ".jpg";
		        $this->thumbname = $this->pasta.$this->id. ".jpg";
		        chmod($this->pasta, 0777);
		        
		        list($this->width, $this->height) = getimagesize($this->filename);
		        
		        $this->image_p = imagecreatetruecolor($this->largura_thumb, $this->altura_thumb);
		        $this->image = imagecreatefromjpeg($this->filename);
		        imagecopyresampled($this->image_p, $this->image, 0, 0, 0, 0, $this->largura_thumb, $this->altura_thumb, $this->width, $this->height);
		        
		        imagejpeg($this->image_p, $this->thumbname, 100);
		        imagedestroy($this->image_p);
		    }
		}
		if((isset($_FILES['foto'.$i]['tmp_name']) and substr($_FILES['foto'.$i]['name'], -3) != "jpg")){
			$this->erro .= "A imagem ".$_FILES['foto'.$i]['name']." não foi gravada pois não é do formato .jpg <br />";
		}

		if($this->erro == ""){
			return "ok";
		}else{
			return $this->erro;
		}
	}

	function gravaDados(){

		switch ($this->tipo){
		    case "insert":
		        $sql = "insert into ".$this->tabela."
		                    (link, texto, categoria)
		                values
		                    ('".$this->link."', '".$this->texto."', ".$this->categoria.")";
		    break;
		    case "update":
		        $sql = "update ".$this->tabela."
		                    set link = '".$this->link."', texto = '".$this->texto."', categoria = ".$this->categoria."
		                where id = ".$this->id;
		    break;
		}

		$obj = new Conexao;
        $obj->set('sql', $sql);
        if($obj->query()){
        	$this->gravaImagens();
        	return "ok";
        }else{
        	return "Erro ao tentar gravar os dados";
        }
	}
}
?>