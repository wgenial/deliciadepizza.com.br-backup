<?php 
    require_once("style.php");
?>
<headers id="cabecalho" class="cor1">
    <a href="/"><img src="imagens/logo_cliente.png" alt="" title="Home" class="logo" /></a>
    <div id="detalhe_cabecalho" class="borda_detalhe_cabecalho">
        PAINEL DE CONTROLE
        <div class="ip cor_fonte1">
            Seu IP é <?php echo IP; ?>
        </div>
    </div>
</headers>

<section id="informacoes_sistema" class="cor2 cor_fonte1">
    <img src="imagens/ico_usuario.png" alt="ico_usuario" />
    Seja bem-vindo <b><?php echo NOME; ?></b> ao <b>painel de controle</b>. Navegue pelo menu abaixo para efeturar as alterações.
    <nav id="links_sistema">
        <a href="<?php echo DOMINIO; ?>" class="cor_fonte1" target="_blank">Visitar Site</a> | 
        <a href="configuracoes_formulario.php" class="cor_fonte1">Editar Conta</a> | 
        <a href="index.php" class="cor_fonte1">Logout</a> 
    </nav>
</section>
