﻿<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8" />
<meta name="robots" content="noindex, nofollow">

<link rel="stylesheet" href="css/geral.css">
<link rel="stylesheet" href="css/menu_esquerdo.css">
<link rel="stylesheet" href="cleditor/jquery.cleditor.css">
<link rel="shortcut icon" href="imagens/favicon.ico" type="image/x-icon" />

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="cleditor/jquery.cleditor.completo.js"></script>
<script type="text/javascript" src="crop/jquery.imgareaselect.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#texto").cleditor()[0].focus();
    });
</script>
<title><?php echo CLIENTE; ?> - Início</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<div id="moldura" class="borda_aside">
        <?php 
            require("headers.tpl");
            require("menu_esquerdo.tpl");
        ?>
    <section id="conteudo">
        <h1>
            <img src="imagens/h1_internas.jpg" alt="Icone Internas" />
            SEJA BEM-VINDO
        </h1>
    </section>
    </div><!-- fim da moldura -->
    <?php require_once("footer.tpl"); ?>
</body>
</html>