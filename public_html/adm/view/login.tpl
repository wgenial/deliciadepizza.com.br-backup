﻿
<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8" />
<meta name="robots" content="noindex, nofollow">

<link rel="stylesheet" href="css/geral.css">
<link rel="stylesheet" href="css/menu_esquerdo.css">
<link rel="shortcut icon" href="imagens/favicon.ico" type="image/x-icon" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<title><?php echo CLIENTE; ?> - Login</title>
</head>

<body>
	<div id="moldura" class="borda_aside">
    	<?php require_once("headers_off.tpl"); ?>
        <?php
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            require_once("controller/login.php");
        }
        ?>
        <form name="entrar" id="entrar" method="post" action="" class="box_login borda_aside">
        	<h2 style="text-align:center; margin-bottom:0;">ÁREA RESTRITA</h2>
            <div class="linha_formulario_login">
            	<div class="label_formulario" style="width:60px; margin-left:60px;">LOGIN</div>
                <input type="text" name="login" id="login" />
            </div>
            <div class="linha_formulario_login">
            	<div class="label_formulario" style="width:60px; margin-left:60px;">SENHA</div>
                <input type="password" name="senha" id="senha" />
            </div>
            <input type="submit" name="enviar" id="enviar" value="" class="botao bt_enviar" style="margin-right:60px; float:right;" />
            <div style="text-align:center; float:left; width:100%; margin-top: -10px;">
            	<a href="resgatar_senha.php" class="cor_fonte4">Não lembro minha senha</a>
            </div>
        </form> 
    </div><!-- fim da moldura -->
</body>
</html>
