<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8" />
<meta name="robots" content="noindex, nofollow">

<link rel="stylesheet" href="css/geral.css">
<link rel="stylesheet" href="css/menu_esquerdo.css">
<link rel="stylesheet" href="cleditor/jquery.cleditor.css">
<link rel="shortcut icon" href="imagens/favicon.ico" type="image/x-icon" />

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="cleditor/jquery.cleditor.completo.js"></script>
<script type="text/javascript" src="crop/jquery.imgareaselect.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#texto").cleditor()[0].focus();
	});
</script>
<title><?php echo CLIENTE; ?> - Notícia</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<div id="moldura" class="borda_aside">
    	<?php 
			require_once("headers.tpl");
			require_once("menu_esquerdo.tpl");

			if(isset($_REQUEST["id"]) and $_REQUEST["id"] != "" ){
					
				$pasta = "../noticias";
				$id = mysql_real_escape_string($_REQUEST["id"]);

				$sql = "select * from ".PREFIXO."noticias where id = $id";
				
				$obj->set('sql', $sql);
				$res = $obj->query();
				$rs = mysql_fetch_array($res);
				
				$titulo = $rs["titulo"];
				$texto = $rs["texto"];
				$categoria = $rs["categoria"];
				$dataEntrada = $rs["data_entrada"];
				$dataSaida = $rs["data_saida"];
				$thumb = "";
				
				if(file_exists($pasta."/".$id.".gif")){
					$thumb = $pasta."/".$id.".gif";
				}
				if(file_exists($pasta."/".$id.".png")){
					$thumb = $pasta."/".$id.".gif";
				}
				if(file_exists($pasta."/".$id.".jpg")){
					$thumb = $pasta."/".$id.".jpg";
				}
						   
			}
			?>
<section id="conteudo">
    <h1>
        <img src="imagens/h1_internas.jpg" alt="Icone Internas" />
        NOTÍCIAS
    </h1>
    <?php
    require_once("controller/thumb.php");
    if(substr($thumb_image_location, -4) == ".jpg" or substr($thumb_image_location, -4) == ".gif" or substr($thumb_image_location, -4) == ".png"){
       $thumb_noticia = $thumb_image_location;
    }
    $thumb_noticia = $_POST[''];

    if($_SERVER['REQUEST_METHOD'] == 'POST' and $_POST['titulo'] != ""){
        require_once("controller/noticias.php");
    }
    ?>

    <form id="noticia" name="noticia" method="post" enctype="multipart/form-data" action="">
        <input type="hidden" name="id" id="id" value="<?php echo $id; ?>" />
        <input type="hidden" name="imagem" id="imagem" value="<?php echo $thumb_image_location; ?>" required />
        <div class="linha_formulario">
            <div class="label_formulario">CATEGORIA</div>
            <select name="categoria">
                <?php
                $sql = "select * from ".PREFIXO."categorias_noticias order by titulo asc";
                
                $obj->set('sql', $sql);
                $res = $obj->query();
                
                criar_options($res, "id", $categoria, "titulo");
                ?>
            </select>
        </div>
        <?php if($thumb != ""){ ?>
        <div class="linha_formulario">
            <div class="label_formulario">THUMB PREVIAMENTE GRAVADO</div>
            <img src="<?php echo $thumb; ?>" alt="" title="" />
        </div>
        <?php } ?>
        <div class="linha_formulario">
            <div class="label_formulario">TÍTULO</div>
            <input type="text" name="titulo" id="titulo" title="Clique para editar" value="<?php echo $titulo; ?>" required />
        </div>
        <div class="linha_formulario">
            <div style="float:left">
                <div style="200px; font-size:10px;">DATA DE PUBLICAÇÃO</div>
                <input type="text" name="data_entrada" id="data_entrada" style="width:200px" value="<?php echo $data_entrada; ?>" required />
            </div>
            <div style="float:left; margin-left:20px; font-size:10px;">
                <div style="200px">DATA SAÍDA</div>
                <input type="text" name="data_saida" id="data_saida" style="width:200px" title="Clique para editar" value="<?php echo $data_saida; ?>" required />
            </div>
        </div>
        <div class="linha_formulario_cleditor">
            <div class="label_formulario">TEXTO</div>
            <textarea name="texto" id="texto" required ><?php echo $texto; ?></textarea>
        </div>
        <div class="div_dois_botoes">
            <input type="button" value="" id="cancelar" name="cancelar" class="botao bt_cancelar" onclick="window.location='inicio.php'" />
            <!--<input type="button" value="" id="visualizar" name="visualizar" class="botao bt_visualizar" />-->
            <input type="submit" value="" id="publicar" name="publicar" class="botao bt_publicar" />
        </div>
    </form>

</section>
    </div><!-- fim da moldura -->
	<?php require_once("footer.tpl"); ?>
</body>
</html>
