<?php session_start(); 
require_once("classes.php");
require_once("config.php");

$log = new Logado;
$log->set('login', $_SESSION["login"]);
$log->Sessao($_SESSION["login"], $_SESSION["ativo"]);

$titulo_pagina = "Notícias";
$nome = $_SESSION["nome"];
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8" />
<meta name="robots" content="noindex, nofollow">

<link rel="stylesheet" href="css/geral.css">
<link rel="stylesheet" href="css/menu_esquerdo.css">
<link rel="stylesheet" href="cleditor/jquery.cleditor.css">
<link rel="shortcut icon" href="imagens/favicon.ico" type="image/x-icon" />

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="cleditor/jquery.cleditor.completo.js"></script>
<script type="text/javascript" src="crop/jquery.imgareaselect.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#texto").cleditor()[0].focus();
	});
</script>
<?php require_once("style.php"); ?>
<title><?php echo CLIENTE . " - ".$titulo_pagina; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<div id="moldura" class="borda_aside">
    	<?php 
			require_once("partes/headers.php");
			require_once("partes/menu_esquerdo.php");
			require_once("partes/formulario_noticias.php");
		?>
    </div><!-- fim da moldura -->
	<?php require_once("partes/footer.php"); ?>
</body>
</html>
