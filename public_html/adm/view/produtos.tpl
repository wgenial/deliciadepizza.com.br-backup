<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8" />
<meta name="robots" content="noindex, nofollow">

<link rel="stylesheet" href="css/geral.css">
<link rel="stylesheet" href="css/menu_esquerdo.css">
<link rel="stylesheet" href="cleditor/jquery.cleditor.css">
<link rel="shortcut icon" href="imagens/favicon.ico" type="image/x-icon" />

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="cleditor/jquery.cleditor.completo.js"></script>
<script type="text/javascript" src="crop/jquery.imgareaselect.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#texto").cleditor()[0].focus();
    });
</script>

<title><?php echo CLIENTE; ?> - Início</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>

<body>
<div id="moldura" class="borda_aside">
        <?php 
            require("headers.tpl");
            require("menu_esquerdo.tpl");
            require("controller/produtoEditar.php")
        ?>

<section id="conteudo">
    <h1>
        <img src="imagens/h1_internas.jpg" alt="Icone Internas" />
        PRODUTOS
    </h1>
        <?php
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            require_once("controller/produtos.php");
        }
        ?>
    <form id="produto" name="produto" method="post" enctype="multipart/form-data" action="#">
        <input type="hidden" name="id" id="id" value="<?php echo $_GET["id"]; ?>" />
        <div class="linha_formulario">
            <div class="label_formulario">CATEGORIA</div>
            <select name="categoria">
                <?php
                $sql = "select * from ".PREFIXO."categorias_produtos order by titulo asc";
                
                $obj->set('sql', $sql);
                $res = $obj->query();
                
                criarOptions($res, "id", $produto->categoria, "titulo");
                ?>
            </select>
        </div>
        <div class="linha_formulario">
            <div class="label_formulario">TÍTULO</div>
            <input type="text" name="titulo" id="titulo" title="Clique para editar" value="<?php echo $produto->titulo; ?>" required />
        </div>
        <div class="linha_formulario">
            <div class="label_formulario">PREÇO</div>
            <input type="text" name="preco" id="preco" title="Clique para editar" value="<?php echo $produto->preco; ?>" required style="width:80px" />
        </div>
        <div class="linha_formulario_cleditor">
            <div class="label_formulario">TEXTO</div>
            <textarea name="texto" id="texto" required ><?php echo $produto->texto; ?></textarea>
        </div>
        <!--
        <?php
        for($i=1; $i<6; $i++){
        ?>
        <div class="linha_formulario">
            <div class="label_formulario">FOTO <?php echo $i; ?></div>
            <input type="file" name="foto<?php echo $i; ?>" id="foto<?php echo $i; ?>" />
        </div>
        <?php
        }
        ?>
        -->
        <div class="div_dois_botoes">
            <input type="button" value="" id="cancelar" name="cancelar" class="botao bt_cancelar" onclick="window.location='inicio.php'" />
            <!--<input type="button" value="" id="visualizar" name="visualizar" class="botao bt_visualizar" />-->
            <input type="submit" value="" id="publicar" name="publicar" class="botao bt_publicar" />
        </div>
    </form>
</section>    </div><!-- fim da moldura -->
    <?php require_once("footer.tpl"); ?>
</body>
</html>