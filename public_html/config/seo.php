<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35350521-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<?php
$rota = isset($_GET["rota"]) ? $_GET["rota"] : "";
$explode = explode("/", $rota);
$rota = isset($explode[1]) ? $explode[1] : "";
$nome = "Delícia de Pizza";
$dominio = "http://www.deliciadepizza.com.br";

switch ($rota){
	case "nossaCasa":
		$titulo = "Nossa Casa | ".$nome;
		$description = "Delícia de Pizza. Elaboramos as nossas pizzas com ingredientes de primeira qualidade, para garantir o resultado final ainda mais leve e crocante. 
		Oferecemos uma ampla variedade de sabores, totalizando mais de 60 tipos de pizzas salgadas e doces";
		$keywords = "Delícia de Pizza, pizzas, pedidos, on-line, variedade";
		$cannonical = $dominio."/nossa-casa";
	break;
	case "cardapio":
		$titulo = "Cardápio de Pizzas, Pizzas Doces, Sobremesas, Bebidas| ".$nome;
		$description = "Confira aqui nosso cardápio de pizzas, pizzas doces, sobremesas, bebidas. Delícia de Pizza.";
		$keywords = "pizza, pizzas, doce, sobremesa, refrigerante, cerveja, vinho";
		$cannonical = $dominio."/cardapio";
	break;
	case "cadastro":
		$titulo = "Cadastro | ".$nome;
		$description = "Cadastro Delícia de Pizza. Participe das promoções e receba novidades. Pizza é na Delícia de Pizza.";
		$keywords = "cadastro, on-line, pedido, entrega, Delícia de Pizza";
		$cannonical = $dominio."/cadastro";
	break;
	case "contato":
		$titulo = "Contato | ".$nome;
		$description = "A Delícia de Pizza disponibiliza neste espaço seus contatos: Formulário web, e-mail, telefone. Fale conosco, será um prazer atendê-lo.";
		$keywords = "contato, on-line, email, telefone, Delícia de Pizza, atendimento, Delícia de Pizza";
		$cannonical = $dominio."/fale-conosco";
	break;
	case "delivery":
		$titulo = "Delivery | ".$nome;
		$description = "Maior comodidade. Nossas entregas são feitas pela equipe delivery, composta de profissionais selecionados, dotados de capacitação, experiência 
		e responsabilidade, comprometidos com a satisfação do cliente";
		$keywords = "contato, entrega, delivery, satisfação, pizza, pizzaria.";
		$cannonical = $dominio."/delivery";
	break;
	case "enderecos":
		$titulo = "Endereços | ".$nome;
		$description = "Escolha o mais perto de você. Capital: Vila Leopoldina, Vila Mariana, Panamby Open Mall, Jardim Anália Franco e Aclimação. Jundiaí: Avenida Nove de Julho, ao lado do Burger King";
		$keywords = "endereço, enderecos, telefone, leopoldina, mariana, pananby, anália franco, aclimação, jundiaí, delícia, pizza, burger king, delícia de pizza.";
		$cannonical = $dominio."/enderecos";
	break;

	default:
		$titulo = "Pizzaria | ".$nome;
		$description = "Delícia de Pizza. Elaboramos as nossas pizzas com ingredientes de primeira qualidade, para garantir o resultado final ainda mais leve e crocante. 
		Oferecemos uma ampla variedade de sabores, totalizando mais de 60 tipos de pizzas salgadas e doces";
		$keywords = "Delícia de Pizza, pizzas, pedidos, on-line, variedade";
		$cannonical = $dominio."/nossa-casa";

}
?>