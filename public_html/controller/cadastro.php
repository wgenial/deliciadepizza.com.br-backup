<?php 
require("model/email.class.php");
require("model/cadastro.class.php");

$nome = isset($_POST["nome"]) ? mysql_real_escape_string($_POST["nome"]) : "";
$email = isset($_POST["email"]) ? mysql_real_escape_string($_POST["email"]) : "";
$cep = isset($_POST["cep"]) ? mysql_real_escape_string($_POST["cep"]) : "";
$endereco = isset($_POST["endereco"]) ? mysql_real_escape_string($_POST["endereco"]) : "";
$numero = isset($_POST["numero"]) ? mysql_real_escape_string($_POST["numero"]) : "";
$bairro = isset($_POST["bairro"]) ? mysql_real_escape_string($_POST["bairro"]) : "";
$complemento = isset($_POST["complemento"]) ? mysql_real_escape_string($_POST["complemento"]) : "";
$cidade = isset($_POST["cidade"]) ? mysql_real_escape_string($_POST["cidade"]) : "";
$uf = isset($_POST["uf"]) ? mysql_real_escape_string($_POST["uf"]) : "";
$recebeNews = isset($_POST["recebeNews"]) ? mysql_real_escape_string($_POST["recebeNews"]) : "";

if($nome == "" or $email == ""){
	echo "Todos os campos são obrigatórios";
	exit;
}

$emailPadrao = "faleconosco@pizzariabonanova.com.br";
$titulo = "Cadastro - Site";

$corpo = "Nome: $nome <br /> \r\n
		  E-mail: $email <br /> \r\n
		  CEP: $cep <br /> \r\n
		  Endereço: $endereco, $numero <br /> \r\n
		  Complemento: $complemento <br /> \r\n
		  Bairro: $bairro <br /> \r\n
		  Cidade: $cidade - $uf";


//$contato = new Email($emailPadrao, $titulo, $corpo);

if($recebeNews != ""){
	$cadastro = new Cadastro($nome, $email, '',$cep, $endereco, $numero, $complemento, $bairro, $cidade, $uf, 1, date("Y-m-d"), '', 1);
}else{
	$cadastro = new Cadastro($nome, $email, '',$cep, $endereco, $numero, $complemento, $bairro, $cidade, $uf, 0, date("Y-m-d"), '', 1);
}
echo $cadastro->resposta;
?>
