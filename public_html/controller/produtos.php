<?php 
require_once("model/listaProdutos.class.php");

function pegaId($arquivo){
	$explode = explode("-", $arquivo);
	if(isset($explode[1])){
		return str_replace(".tpl", "", $explode[1]);
	}else{
		return '';
	}
}

$id = pegaId($_GET["rota"]);

//$id = isset($_GET["id"]) ? mysql_real_escape_string($_GET["id"]) : "";
if($id != ""){

	$tamanhoPagina = 200;

	$pagina = isset($_GET["pagina"]) ? mysql_real_escape_string($_GET["pagina"]) : 0;
	$inicioPagina = (($pagina * $tamanhoPagina) - $tamanhoPagina);
	if($inicioPagina < 0){
	    $inicioPagina = 0;
	}

	$produto = new Produtos;

	$produto->set("categoria", $id);
	$produto->set("paginacao", $inicioPagina.", ".$tamanhoPagina);

	$produto->consultarProdutos();
}else{
	$produto = new Produtos;
	$produto->erro = "";
	$produto->total = -1;
}

?>
