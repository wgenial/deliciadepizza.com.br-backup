<?php 
require("model/email.class.php");
require("model/trabalhe.class.php");
require("includes/funcoes.php");

$nome = isset($_POST["nome"]) ? ($_POST["nome"]) : "";
$email = isset($_POST["email"]) ? ($_POST["email"]) : "";
$telefone = isset($_POST["telefone"]) ? ($_POST["telefone"]) : "";
$mensagem = isset($_POST["mensagem"]) ? ($_POST["mensagem"]) : "";
$anexo = isset($_FILES["anexo"]["name"]) ? ($_FILES["anexo"]["name"]) : "";
$anexo = trocaacento($anexo);
$anexo = semEspaco($anexo);

if($nome == "" or $email == "" or $mensagem == "" or $anexo == ""){
	echo "Todos os campos são obrigatórios";
	exit;
}

if(substr($anexo, -3) != "doc"){

	if(substr($anexo, -3) != "ocx"){
		echo "Só aceitamos arquivos doc e docx";
		exit;
	}

}

$emailPadrao = "faleconosco@deliciadepizza.com.br";
$titulo = "Trabalhe Conosco - Site";

$corpo = "Nome: $nome <br /> \r\n
		  E-mail: $email <br /> \r\n
		  Telefone: $telefone  <br /> \r\n
		  CV: http://www.deliciadepizza.com.br/cv/". $anexo."  <br /> \r\n
		  Mensagem: $mensagem";

$contato = new Email($emailPadrao, $titulo, $corpo);

	$trabalhe = new Trabalhe($anexo); // 1 recebe news

echo $trabalhe->resposta;

?>
