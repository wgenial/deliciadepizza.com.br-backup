/*
*/
// Cria o evento Push State
(function (history) {
    "use strict";
    var pushState = history.pushState;
    history.pushState = function (state) {
        if (typeof history.onpushstate === "function") {
            history.onpushstate({state: state});
        }
        // ... whatever else you want to do
        // maybe call onhashchange e.handler
        return pushState.apply(history, arguments);
    };

}) (window.history);

// Objetos Globais
var objMenu = {     
    strUrlPrefix : 'delicia',
    strGet : '?rota=view/',
    ieIndex: 'http://www.deliciadepizza.com.br/#!/'
}

var objEndereco = {
    PaginaAtual: '',
    PaginasAnteriores: ''
}
var contador = 0;

//fim objetos Globais

// Espera os scripts serem carregados
$(document).ready(function($) {

    // evento Click em qualquer elemento do menu
    $("#menuSuperior ul li a").click(function (e) {
        e.preventDefault();        
        var urlFinal = trataURL($(this).attr('href'));   

        mudarEndereco ( urlFinal );
    });
    $("#menuSuperior ul li").click(function (e) {
        e.preventDefault();        
        var urlFinal = trataURL($(this).find("a").attr('href'));  

        mudarEndereco ( urlFinal );
    });

    $("#menuSuperior ul li a").hover(function (e) {
        animaMenu(this.id);
    });
    $("#menuSuperior ul li").mouseleave(function (e) {
       zerarContador($(this).find("a").attr("id"));
    });

});

function zerarContador(campo){
    var campo = $("#" + campo);
    contador = 0;
    campo.css({"color":"#ffffff"}).animate({"top" : "-25px"}, 200).animate({"top" : "25px"}, 0).animate({"top" : "0px"}, 200);
}

function animaMenu(campo){
    var campo = $("#" + campo);
    if(contador == 0 && campo.css({"top": "0px"})){
        contador++;
        campo.css({"top":"0px"}).css({"color":"#fdbf2f"}).animate({"top" : "25px"}, 200).animate({"top" : "-25px"}, 0).animate({"top" : "0px"}, 200);
    }
}

// Retira todo o lixo da url e retorna o get
function trataURL ( url ) {
    var linkHref = url;                // Recebe o href do menu;
    var hrefPartes = linkHref.split(objMenu.strGet);           // divide o endereço em partes;
    var urlFinal = hrefPartes[hrefPartes.length - 1] ;  // recebe o final do Get tratado para ser passado como URL;
    if(urlFinal.indexOf("&id=") > 0){
        urlFinal = urlFinal.replace("&id=", "-");
    }
    return urlFinal;
}

// função que checa como deve ser feita a mudança de endereço
function mudarEndereco (url) {
    if ( $('html').hasClass('ie9') || $('html').hasClass('ie8') || $('html').hasClass('ie7') || $('html').hasClass('ie6') ) {
        var urlAtual = new String( trataURL(String(window.location) ) );
        if ( urlAtual.match( /http/gi) ){   // Se o usuário chegou ao site com IE9- e chegou no index
            mudarEnderecoIE ( url );        // comportamento normal de mudança de endereço
        } else {
            window.location( objMenu.ieIndex + url); // Se o usuário não chegou pelo index, redireciona o usuário para o index e substitui o valor da Hash de acordo com o link clicado
        }        
    } else {
        mudarEnderecoHTML5 ( url );
    }
}
// função para trocar o endereço no IE
function mudarEnderecoIE ( url ) {
    $.address.crawlable(true).value(url);
}
// função para trocar o endereço nos browsers compativeis com pushState do html5
function mudarEnderecoHTML5 ( url ) {
    if ( objEndereco.PaginaAtual != url ) {
        objEndereco.PaginaAtual = url;
        window.history.pushState(objEndereco,  "Title", url);
    }    
}
// fim da checagem de endereço

// Unificando os eventos dos browsers :
window.onpopstate = history.onpushstate = function(event) {
    if (event.state != null){
        objEndereco.PaginaAtual = event.state.PaginaAtual;
        navegarParaPagina(event.state.PaginaAtual);
    }
};
$.address.change(function(event) {
    navegarParaPagina ( (event.value).substring(1) );
});
// Fim dos eventos

// Funções de navegação, loading/animações, ajax
function navegarParaPagina (pagina) {
    if (pagina != '') {
        limparAreaUtil(pagina);
    }
}

function limparAreaUtil (pagina) {
    var tela = screen.width;
    var negativo = "-" + tela + "px";
    var positivo = tela + "px";

    if($("#conteudo").css({"margin-left":"0px"})){
        $("#conteudo").animate({"margin-left": negativo}, 500).animate({"margin-left" : positivo}, 0, function(){carregaPagina(pagina);});
    }
}

function carregaPagina (pagina) {
    var getPagina = 'main.php?rota=view/' + pagina + '.tpl';
    $("#conteudo").html("Carregando..."); 

    $.get(
        getPagina, 
        function(data){
            $("#conteudo").html(data); 
            $("#conteudo").animate({"margin-left":"0px"}, 500)

            $(".menuLateral a").click(function (e) {
                e.preventDefault();
                var urlFinal = trataURL($(this).attr('href'));        
                mudarEndereco ( urlFinal );
            });
        }
    );
}