<?php
function carregaPagina ( $rota ){
	if ( testaGet ( $rota ) ) {
		$arquivo = desmembraUrl($_GET[$rota]);
		require_once($arquivo);
	}else {
		require('view/banner.tpl');
	}
}

function testaGet ( $get ) {
	$arquivo = desmembraUrl($_GET[$get]);
	if (isset($arquivo)) {
	    if(file_exists($arquivo)){
	        return true ;
	    }else{
	        return false;
	    }
	} else {
	    return false;
	}
}

function desmembraUrl($arquivo){
	$explode = explode("-", $arquivo);
	if(isset($explode[1])){
		return $explode[0].".tpl";
	}else{
		return $arquivo;
	}
}

carregaPagina ( 'rota' );
?>