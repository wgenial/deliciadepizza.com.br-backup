<?php
require_once("model/banners.class.php");

$nome = $_SESSION["nome"];
$acesso = $_SERVER['REQUEST_URI'];

//Eu quero criar um controle para que o endereço que enviou a informação seja checado.
if($acesso != "/banners.php"){
    //echo $acesso;
}

$pasta = "../banners/";
$tabela = PREFIXO."banners";

if(isset($_POST["id"]) and $_POST["id"] != ""){
    $id = $_POST["id"];
    $tipo = "update";
}else{
    $id = "";
    $tipo = "insert";
}

$texto = isset($_POST["texto"]) ? $_POST["texto"] : "";
$link = isset($_POST["link"]) ? $_POST["link"] : "";
$categoria = isset($_POST["categoria"]) ? $_POST["categoria"] : "";

obrigatorio($link, "Link");
obrigatorio($texto, "Texto");
obrigatorio($categoria, "Categoria");

$produto = new Produtos;

$produto->set("categoria", $categoria);
$produto->set("texto", $texto);
$produto->set("link", $link);
$produto->set("tipo", $tipo);
$produto->set("pasta", $pasta);
$produto->set("tabela", $tabela);
$produto->set("id", $id);


if($produto->gravaDados() == "ok"){
    echo "<span style='color:green'>Dados Gravados com sucesso.</span>";
    exit;
}else{
	echo "<span style='color:red'>Não foi possível gravar essas informações</span>";
	exit;
}
?>