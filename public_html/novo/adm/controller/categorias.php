<?php
require_once("model/categorias.class.php");

$nome = $_SESSION["nome"];
$acesso = $_SERVER['REQUEST_URI'];

//Eu quero criar um controle para que o endereço que enviou a informação seja checado.
if($acesso != "/categorias.php"){
    //echo $acesso;
}

$pasta = "../categorias/";
$tabela = PREFIXO."categorias_produtos";

if(isset($_POST["id"]) and $_POST["id"] != ""){
    $id = $_POST["id"];
    $tipo = "update";
}else{
    $id = "";
    $tipo = "insert";
}

$titulo = isset($_POST["titulo"]) ? $_POST["titulo"] : "";

obrigatorio($titulo, "Título");

$produto = new Categorias;

$produto->set("titulo", $titulo);
$produto->set("tipo", $tipo);
$produto->set("pasta", $pasta);
$produto->set("id", $id);
$produto->set("tabela", $tabela)


if($produto->gravaDados() == "ok"){
    echo "<span style='color:green'>Dados Gravados com sucesso.</span>";
    exit;
}else{
	echo "<span style='color:red'>Não foi possível gravar essas informações</span>";
	exit;
}
?>