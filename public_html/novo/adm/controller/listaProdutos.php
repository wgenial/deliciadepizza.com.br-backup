<?php
require_once("model/listaProdutos.class.php");

$tamanhoPagina = 200;

$pagina = isset($_GET["pagina"]) ? mysql_real_escape_string($_GET["pagina"]) : 0;
$inicioPagina = (($pagina * $tamanhoPagina) - $tamanhoPagina);
if($inicioPagina < 0){
	$inicioPagina = 0;
}

$h1 = "Produtos";
$pasta  = "produtos";

$lista = new Produto;

$lista->set("paginacao", $inicioPagina.", ".$tamanhoPagina);

$lista->consultarProdutos();
?>