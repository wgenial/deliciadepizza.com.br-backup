<?php 
require_once("model/noticias.class.php");
$acesso = $_SERVER["PHP_SELF"];

if($acesso != "index.php"){
	echo $acesso;
	exit;
}else{
	echo "Acessou do lugar certo";
}

$pasta = "../noticias/";
$tabela = PREFIXO."noticias";

if(isset($_POST["id"]) and $_POST["id"] != ""){
	$id =  $_POST["id"];
	$tipo = "update";
}else{
	$id =  "";
	$tipo = "insert";
}

$categoria = isset($_POST["categoria"]) ? mysql_real_escape_string($_POST["categoria"]) : "";
$titulo = isset($_POST["titulo"]) ? mysql_real_escape_string($_POST["titulo"]) : "";
$texto = isset($_POST["texto"]) ? mysql_real_escape_string($_POST["texto"]) : "";
$texto = trim($texto);
$dataEntrada = isset($_POST["data_entrada"]) ? converte_data($_POST["data_entrada"]) : "0000-00-00";
$dataSaida = isset($_POST["data_saida"]) ? converte_data($_POST["data_saida"]) : "0000-00-00";
$imagem = isset($_POST["imagem"]) ? $_POST["imagem"] : "";
if(strlen($imagem) > 4){
	$extensao = substr($imagem, -4);
	if($extensao = "jpeg"){
		$extensao = ".jpg";	
	}
}

obrigatorio($titulo, "Título");
obrigatorio($texto, "Texto");
obrigatorio($categoria, "Categoria");
obrigatorio($imagem, "Imagem");

$noticia = new Noticia;

$noticia->set('tabela', $tabela);
$noticia->set('pasta', $pasta);
$noticia->set('categoria', $categoria);
$noticia->set('titulo', $titulo);
$noticia->set('texto', $texto);
$noticia->set('dataEntrada', $dataEntrada);
$noticia->set('dataSaida', $dataSaida);
$noticia->set('imagem', $imagem);
$noticia->set('extensao', $extensao);

$noticia->gravarNoticia($tipo);
echo $imagem;
?>
