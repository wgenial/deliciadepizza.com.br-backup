<?php
require_once("../includes/conexao.php");

class Produtos {

	private $largunta_thumb;
	private $altura_thumb;
	private $tipo;
	private $tabela;
	private $titulo;
	private $preco;
	private $texto;
	private $categoria;
	private $id;
	private $pasta;
	private $proximo;

	function set($nome, $valor){
		$this->$nome = $valor;
	}

	function dadosImagens(){
		$obj = new Conexao;

		//As medidas das imagens estão na tabela de categorias
		$sql = "select * from ".PREFIXO."categorias_produtos where id = ".$this->categoria;

		$obj->set('sql', $sql);
		$res = $obj->query();
		$rs = mysql_fetch_array($res);

		$this->largura_thumb = $rs["largura"];
		$this->altura_thumb = $rs["altura"];

		//Precisamos do id para gravar as imagens neste padrão
        $sqlId = "select id from ".$this->tabela." where titulo = '".$this->titulo."' order by id desc";

        $obj->set('sql', $sqlId);
        $res = $obj->query();
        $rs = mysql_fetch_array($res);
        $this->id = $rs["id"];

        //Descobrindo qual é o última imagem (_$i) com esse id que está na pasta
		$proximo = 0;
		for($i=1; $i<1000; $i++){
		    if(file_exists($this->pasta."/".$this->id."_".$i. ".jpg")){
		        $this->proximo = $i;  
		    }
		}
	}

	function gravaImagens(){

		$this->dadosImagens();
		$this->erro = "";

		//Só recebemos 20 imagens por envio de formulário. Não queremos uma conexão muito pesada, não é?
		for($i=1; $i<21; $i++){
			if(isset($_FILES['foto'.$i]['tmp_name']) and substr($_FILES['foto'.$i]['name'], -3) == "jpg"){
			    if(move_uploaded_file($_FILES['foto'.$i]['tmp_name'], $this->pasta."/".$this->id."_".($i+$this->proximo). ".jpg")){

			        $this->filename = $this->pasta."/".$this->id."_".($i+$this->proximo). ".jpg";
			        $this->thumbname = $this->pasta."thumbs/".$this->id."_".($i+$this->proximo). ".jpg";
			        chmod($this->pasta, 0777);
			        
			        list($this->width, $this->height) = getimagesize($this->filename);
			        
			        $this->image_p = imagecreatetruecolor($this->largura_thumb, $this->altura_thumb);
			        $this->image = imagecreatefromjpeg($this->filename);
			        imagecopyresampled($this->image_p, $this->image, 0, 0, 0, 0, $this->largura_thumb, $this->altura_thumb, $this->width, $this->height);
			        
			        imagejpeg($this->image_p, $this->thumbname, 100);
			        imagedestroy($this->image_p);
			    }
			}
			if((isset($_FILES['foto'.$i]['tmp_name']) and substr($_FILES['foto'.$i]['name'], -3) != "jpg")){
				$this->erro .= "A imagem ".$_FILES['foto'.$i]['name']." não foi gravada pois não é do formato .jpg <br />";
			}
		}
		if($this->erro == ""){
			return "ok";
		}else{
			return $this->erro;
		}
	}

	function gravaDados(){

		switch ($this->tipo){
		    case "insert":
		        $sql = "insert into ".$this->tabela."
		                    (titulo, texto, preco, categoria)
		                values
		                    ('".$this->titulo."', '".$this->texto."', '".$this->preco."', ".$this->categoria.")";
		    break;
		    case "update":
		        $sql = "update ".$this->tabela."
		                    set titulo = '".$this->titulo."', texto = '".$this->texto."', preco = '".$this->preco."', categoria = ".$this->categoria."
		                where id = ".$this->id;
		    break;
		}

		$obj = new Conexao;
        $obj->set('sql', $sql);
        if($obj->query()){
        	//$this->gravaImagens();
        	return "ok";
        }else{
        	return "Erro ao tentar gravar os dados";
        }
	}
}
?>