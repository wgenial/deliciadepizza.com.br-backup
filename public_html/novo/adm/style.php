<?php 
	require_once("classes.php");
	
	$sql = "select * from projecto_sistema_cores";
		
	$obj = new Conexao;		
	$obj->set('sql', $sql);
	$res = $obj->query();
	$rs = mysql_fetch_array($res);
		
	$escuro = $rs["cor1"];
	$claro = $rs["cor2"];;

?>
<style type="text/css">
	.cor1{
		color: <?php echo $claro; ?>;
	}
	.cor2{
		color: <?php echo $escuro; ?>;
	}
	.cor_fonte1{
		color: <?php echo $rs["cor_fonte1"]; ?>;
	}
	.cor_fonte2{
		color: <?php echo $rs["cor_fonte2"]; ?>;
	}
	.label_formulario, .cor_fonte3{
		color: <?php echo $rs["cor_fonte3"]; ?>;
	}
	.cor_fonte4{
		color: <?php echo $rs["cor_fonte4"]; ?>;
	}
	.cor1, .titulo_aside{
		background: <?php echo $escuro; ?>;
	}
	.cor2, .icone_aside{
		background: <?php echo $claro; ?>;
	}
	aside .link_aside{
		border-bottom:1px <?php echo $claro; ?> solid;
	}
	.borda_aside, .borda_detalhe_cabecalho{
		border-color: <?php echo $escuro; ?>;
	}
	.borda_detalhe_cabecalho{
		border-color: <?php echo $claro; ?>;
	}
</style>

