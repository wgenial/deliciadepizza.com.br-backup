<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8" />
<meta name="robots" content="noindex, nofollow">

<link rel="stylesheet" href="css/geral.css">
<link rel="stylesheet" href="css/menu_esquerdo.css">
<link rel="stylesheet" href="cleditor/jquery.cleditor.css">
<link rel="shortcut icon" href="imagens/favicon.ico" type="image/x-icon" />

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="cleditor/jquery.cleditor.completo.js"></script>
<script type="text/javascript" src="crop/jquery.imgareaselect.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#texto").cleditor()[0].focus();
    });
</script>

<title><?php echo CLIENTE; ?> - Banners</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>

<body>
<div id="moldura" class="borda_aside">
        <?php 
            require("headers.tpl");
            require("menu_esquerdo.tpl");
            require("controller/bannerEditar.php")
        ?>

<section id="conteudo">
    <h1>
        <img src="imagens/h1_internas.jpg" alt="Icone Internas" />
        PRODUTOS
    </h1>
        <?php
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            require_once("controller/banners.php");
        }
        ?>
    <form id="produto" name="produto" method="post" enctype="multipart/form-data" action="#">
        <input type="hidden" name="id" id="id" value="<?php echo $_GET["id"]; ?>" />
        <div class="linha_formulario">
            <div class="label_formulario">BANNERS</div>
            <select name="categoria">
                <?php
                $sql = "select * from ".PREFIXO."categorias_banners order by titulo asc";
                
                $obj->set('sql', $sql);
                $res = $obj->query();
                
                criarOptions($res, "id", $produto->categoria, "titulo");
                ?>
            </select>
        </div>
        <div class="linha_formulario">
            <div class="label_formulario">LINK</div>
            <input type="text" name="link" id="link" title="Clique para editar" value="<?php echo $produto->link; ?>" required />
        </div>
        <div class="linha_formulario">
            <div class="label_formulario">TEXTO</div>
            <input type="text" name="texto" id="texto" title="Clique para editar" value="<?php echo $produto->texto; ?>" required />
        </div>
        <div class="linha_formulario">
            <div class="label_formulario">IMAGEM <?php echo $i; ?></div>
            <input type="file" name="imagem" id="imagem" />
        </div>
        <div class="div_dois_botoes">
            <input type="button" value="" id="cancelar" name="cancelar" class="botao bt_cancelar" onclick="window.location='inicio.php'" />
            <!--<input type="button" value="" id="visualizar" name="visualizar" class="botao bt_visualizar" />-->
            <input type="submit" value="" id="publicar" name="publicar" class="botao bt_publicar" />
        </div>
    </form>
</section>    </div><!-- fim da moldura -->
    <?php require_once("footer.tpl"); ?>
</body>
</html>