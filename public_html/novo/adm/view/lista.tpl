<?php

$nome = $_SESSION["nome"];
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8" />
<meta name="robots" content="noindex, nofollow">

<link rel="stylesheet" href="css/geral.css">
<link rel="stylesheet" href="css/menu_esquerdo.css">
<link rel="shortcut icon" href="imagens/favicon.ico" type="image/x-icon" />


<title><?php echo CLIENTE . " - ".$titulo_pagina; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<div id="moldura" class="borda_aside">
	<?php 
		require_once("headers.tpl");
		require_once("menu_esquerdo.tpl");

		$tipo = isset($_GET["tipo"])? mysql_real_escape_string($_GET["tipo"]) : '';

		switch ($tipo){
            case "noticias":
                require("controller/listaNoticias.php");
                $img = "sim";
                $data = "sim";
                $texto = "nao";
            break;
            case "produtos":
                require("controller/listaProdutos.php");
                $img = "nao";
                $data = "nao";
                $texto = "sim";
            break;
            case "categoriasProdutos":
                require("controller/listaCategorias.php");
                $img = "nao";
                $data = "nao";
                $texto = "sim";
            break;
            case "banners":
                require("controller/listaBanners.php");
                $img = "sim";
                $data = "nao";
                $texto = "sim";
            break;
            case "categoriasBanners":
                require("controller/listaCategoriasBanners.php");
                $img = "nao";
                $data = "nao";
                $texto = "sim";
            break;
			default:
       			echo "O que gostaria de listar?";
		}
	?>
	<section id="conteudo">
    <h1>
        <img src="imagens/h1_internas.jpg" alt="Icone Internas" />

        <?php echo $h1; ?>
    </h1>
	<?php
    for ($i=0; $i<=$lista->total; $i++ ) {
    ?>
    <article>
        <?php
        if($img == "sim"){
        ?>
    	<figure>
        	<img src="<?php echo imagemTipo("../".$pasta."/".$lista->numero[$i]); ?>" alt="" title="" />
        </figure>
        <?php
        }
        ?>
        <div class="titulo_lista">
        	<a href=""><h1 class="cor_fonte4"><?php echo $lista->titulo[$i]; ?></h1></a>
            <?php
            if($data == "sim"){
            ?>  
            <a href="">
                <h2 class="cor_fonte4">
                    Entrada: <?php echo converte_data($lista->dataEntrada[$i]); ?>
                    Saída: <?php echo converte_data($lista->dataSaida[$i]); ?>
                </h2>
            </a>    
            <?php
            }
            ?>
            <nav>
        		<a href="?rota=view/<?php echo $tipo; ?>&id=<?php echo $lista->numero[$i]; ?>" class="cor_fonte2">Editar Registro</a>
                &nbsp;|&nbsp;	
        		<a href="apagar_noticias.php?id=<?php echo $rs["id"]; ?>" class="cor_fonte2 apagar">Excluir Registro</a>	
            </nav>
        </div>
    </article>
	<?php
	}
    ?>
</section>
 </div><!-- fim da moldura -->
	<?php require_once("footer.tpl"); ?>
</body>
</html>
