<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35622902-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<?php
$rota = isset($_GET["rota"]) ? $_GET["rota"] : "";
$explode = explode("/", $rota);
$rota = isset($explode[1]) ? $explode[1] : "";
$nome = "Delícia de Pizza";
$dominio = "http://www.deliciadepizza.com.br";

switch ($rota){
	case "nossaCasa":
		$titulo = "Nossa Casa | ".$nome;
		$description = "Delícia de Pizza. Saiba mais sobre essa pizzaria instalada na Vila Carrão em São Paulo";
		$keywords = "Delícia de Pizza, pizza, pizzaria, vila, carrão, empresa";
		$cannonical = $dominio."/nossa-casa";
	break;
	case "produtos":
		$titulo = "Produtos | ".$nome;
		$description = "Saiba mais sobre nossos produtos. Fazemos Pizzas, esfihas, entregamos bebidas e contamos com as melhores marcas";
		$keywords = "produtos, pizzas, esfihas, bebidas, entrega, Delícia de Pizza";
		$cannonical = $dominio."/produtos";
	break;
	case "localizacao":
		$titulo = "Mapa de Localização | ".$nome;
		$description = "Confira aqui nosso mapa de localização. Disponibilizamos através do Google Maps nosso endereço.";
		$keywords = "mapa, google, pizzas, vila, carrão, entrega, Delícia de Pizza";
		$cannonical = $dominio."/localizacao";
	break;
	case "cadastro":
		$titulo = "Cadastro | ".$nome;
		$description = "Cadastre-se em nosso banco de dados e faça seu pedido on-line. As melhores opções em Pizza estão na Delícia de Pizza.";
		$keywords = "cadatro, on-line, pedido, vila, carrão, entrega, Delícia de Pizza";
		$cannonical = $dominio."/cadastro";
	break;
	case "contato":
		$titulo = "Contato | ".$nome;
		$description = "A Delícia de Pizza disponibiliza neste espaço seus contatos: Formulário web, e-mail, telefone. Fale conosco, será um prazer atendê-lo.";
		$keywords = "contato, on-line, email, telefone, Delícia de Pizza, atendimento, Delícia de Pizza";
		$cannonical = $dominio."/fale-conosco";
	break;
	case "areaEntrega":
		$titulo = "Área de Entrega | ".$nome;
		$description = "Devido a nossa política de qualidade, só fazemos entregas que estejam numa raio máximo de 5km de distância da pizzaria.";
		$keywords = "contato, on-line, email, telefone, Delícia de Pizza, atendimento, Delícia de Pizza";
		$cannonical = $dominio."/area-entrega";
	break;

	default:
		$titulo = "Pizzaria | ".$nome;
		$description = "Delícia de Pizza. Bem vindo ao site da pizzaria Delícia de Pizza instalada na Vila Carrão em São Paulo";
		$keywords = "home, Delícia de Pizza, pizza, pizzaria, vila, carrão, empresa";
		$cannonical = $dominio."/";

}
?>