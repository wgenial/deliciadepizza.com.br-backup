<?php 
require("model/email.class.php");
require("model/cadastro.class.php");

$nome = isset($_POST["nome"]) ? mysql_real_escape_string($_POST["nome"]) : "";
$email = isset($_POST["email"]) ? mysql_real_escape_string($_POST["email"]) : "";
$assunto = isset($_POST["assunto"]) ? mysql_real_escape_string($_POST["assunto"]) : "";
$mensagem = isset($_POST["mensagem"]) ? mysql_real_escape_string($_POST["mensagem"]) : "";
$recebeNews = isset($_POST["recebeNews"]) ? mysql_real_escape_string($_POST["recebeNews"]) : "";

if($nome == "" or $email == "" or $mensagem == ""){
	echo "Todos os campos são obrigatórios";
	exit;
}

$emailPadrao = "faleconosco@deliciadepizza.com.br";
$titulo = "Contato - Site";

$corpo = "Nome: $nome <br /> \r\n
		  E-mail: $email <br /> \r\n
		  Assunto: $assunto  <br /> \r\n
		  Telefone: $telefone  <br /> \r\n
		  Aniversário: $aniversario  <br /> \r\n
		  Mensagem: $mensagem";

$contato = new Email($emailPadrao, $titulo, $corpo);

if($recebeNews != ""){
	$cadastro = new Cadastro($nome, $email, '','', '', '', '', '', '', '', 1, date("Y-m-d"), '', 1); // 1 recebe news
}else{
	$cadastro = new Cadastro($nome, $email, '','', '', '', '', '', '', '', 0, date("Y-m-d"), '', 1); // 2 não recebe news
}
echo $cadastro->resposta;

?>
