<?php

function carregaPagina ( $rota ){
	if ( testaGet ( $rota ) ) {
		include($_GET[$rota].".tpl");
	}else {
		include('view/home.tpl');
	}
}

function testaGet ( $get ) {
	if (isset($_GET[$get])) {
	    if(file_exists($_GET[$get].".tpl")){
	        return true ;
	    }else{
	        return false;
	    }
	} else {
	    return false;
	}
}

carregaPagina ( 'rota' );
?>