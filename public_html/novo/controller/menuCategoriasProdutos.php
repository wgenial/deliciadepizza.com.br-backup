<?php 
require_once("model/menuCategorias.class.php");

$categoria = new Categorias;
$categoria->set("tabela", "projecto_categorias_produtos");
$categoria->set("ordena", "id");
$categoria->set("lado", "asc");

$categoria->consultarCategorias();
?>