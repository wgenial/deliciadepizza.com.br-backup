$(document).ready(function($) {
	(function (banner, w, h) {
		var paginaAtual = 0;
		var tempoDelay = 4000;
		var tempoAnimacao = 1000;

		//Private vars
		var intervalo = setInterval(animaBannerProximo, tempoDelay);
		var intQtdBanners = banner.children().length;
		//Constructor		
		(function iniciaBanner ( ) {
			paginaAtual = 0;
			
			banner.children().wrapAll('<div class="bannerWraper" />');
			banner.append("<div id=\"bannerNav\"></div>");
			$(".bannerWraper").children('figure').each(function (e) {
				$("#bannerNav").append("<a class=\"bannerItem\" href='#"+ $(this).index() +"'>"+($(this).index()+1)+"</a>");
			});			
			$(".bannerWraper").css({ width: ( intQtdBanners * w) +'px', position: 'absolute' });
			mudaAtual(paginaAtual);
		})();//fim do setup do banner

		// Click event :
		$(".bannerItem").click(function (e) {
			e.preventDefault();
			
			paginaAtual = $(this).index();
			mudaAtual( $(this).index() );
			animaBanner($(".bannerWraper"), paginaAtual);
		});
		// Animação do banner
		function mudaAtual( i ){
			$(".atual").removeClass("atual");
			$("#bannerNav").children().eq(i).addClass("atual");
		}
		function animaBanner(objeto, i) {
			clearInterval(intervalo);
			$(".bannerWraper").stop();
			objeto.animate({left: -(i*w)+'px' }, tempoAnimacao, function ( e ){
				clearInterval(intervalo);
				intervalo = setInterval(animaBannerProximo, tempoDelay);
			});
			paginaAtual = i;
		}
		function animaBannerProximo (){
			paginaAtual ++;
			if ( paginaAtual >= intQtdBanners){
				paginaAtual = 0;
			}
			mudaAtual(paginaAtual);
			animaBanner($(".bannerWraper"), paginaAtual)
		}

	})($('#bannerContainer' ), 900, 462);
});
