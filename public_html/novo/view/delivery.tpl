<div class="faixas"></div>
<article class="fundoEscuro">
    <h1> - Delivery</h1>
    <h6 class="textoDireita" style="margin-top:-60px">maior comodidade</h6>
    <p>
    	<img src="http://dummyimage.com/240x240/ff0/000.jpg" alt="" title="" class="esquerda" style="margin-right:20px;" />
        Nossas entregas são feitas pela equipe delivery, composta de profissionais selecionados, dotados de capacitação, experiência e responsabilidade, comprometidos com a satisfação do cliente.
    <br />
    <br />
        Aqui na Delícia de Pizza, o exigente consumidor paulistano recebe um atendimento de alta qualidade, que supera as suas expectativas. 
    <br />
    <br />
		As entregas são feitas sem nenhum acréscimo, desde que estejam dentro da área delimitada. Para facilitar e trazer maior comodidade aos nossos clientes, os entregadores podem levar consigo equipamentos que possibilitam várias opções de pagamento, como VR Smart, Sodexho, além de cartão de crédito e débito automático.        
    </p>
    <div class="linksDelivery">
    	<a href="" onclick="mudarEndereco ( 'enderecos' )">CONHEÇA NOSSAS UNIDADES</a> &nbsp; &nbsp;<a href="">|</a> &nbsp; &nbsp;<a href="http://www.restauranteweb.com.br/capaRestaurante.cfm?codRestaurante=60&codFilial=1&tipo=cep&cep=00000000&origem=RE&indicaCep=sim1&CFID=27338508&CFTOKEN=69338636" target="_blank">FAÇA SEU PEDIDO ONLINE</a>
    </div>
</article>
<section class="imgDelivery">
    <img src="imagens/detalhePizza.png" alt="" style="margin-top:255px" />
</section>
<div class="faixas"></div>


