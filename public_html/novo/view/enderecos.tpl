<div class="faixas"></div>
<article class="fundoEscuro">
    <h1> - Endereços</h1>
    <h6 class="textoDireita" style="margin-top:-60px">escolha o mais perto de você</h6>
    <div class="meia">

    	<h2 style="margin-top:-20px">VILA LEOPOLDINA</h2>
    	Rua Schiling, 300 <br />
		<b>Fone: (11) 3641-6778 </b> <br />
		<a href="https://maps.google.com.br/maps?q=Rua+Schilling,+300,+S%C3%A3o+Paulo&hl=pt-BR&ie=UTF8&sll=-23.201096,-46.908366&sspn=0.542128,0.891953&oq=Rua+Schiling,+300+&hnear=R.+Schilling,+300+-+Vila+Leopoldina,+S%C3%A3o+Paulo,+05302-000&t=m&z=17&iwloc=A" target="_blank">(Saiba como chegar)</a>

    	<h2>VILA MARIANA</h2>
    	Rua Rio Grande, 85<br />
		<b>Fone: (11) 5084-7575 </b> <br />
		<a href="https://maps.google.com.br/maps?q=Rua+Rio+Grande,+85,+S%C3%A3o+Paulo&hl=pt-BR&ie=UTF8&sll=-23.529803,-46.727733&sspn=0.00845,0.013937&oq=Rua+Rio+Grande,+85&hnear=R.+Rio+Grande,+85+-+Vila+Mariana,+S%C3%A3o+Paulo,+04018-000&t=m&z=17" target="_blank">(Saiba como chegar)</a>

    	<h2>PANAMBY OPEN MALL</h2>
    	R. Antônio da C. Barbosa, s/n°<br />
		<b>Fone: (11) 3776-7776 </b> <br />
		<a href="https://maps.google.com.br/maps?q=Del%C3%ADcia+de+Pizza+Panamby,+Rua+Ant%C3%B4nio+da+Costa+Barbosa+-+Vila+Andrade,+S%C3%A3o+Paulo,+05717-220&hl=pt-BR&ie=UTF8&sll=-23.609138,-46.687689&sspn=0.067557,0.111494&hq=Del%C3%ADcia+de+Pizza&hnear=R.+Ant%C3%B4nio+da+Costa+Barbosa+-+Vila+Andrade,+S%C3%A3o+Paulo,+05717-220&t=m&z=17" target="_blank">(Saiba como chegar)</a>

    </div>
    <div class="meia" style="margin-left:20px">

    	<h2 style="margin-top:-20px">Jd. ANÁLIA FRANCO</h2>
    	Rua Monte Serrat, 1271 <br />
		<b>Fone: (11) 2293-0888 <br /> Nextel ID 55*30*55564</b> <br />
		<a href="https://maps.google.com.br/maps?q=Rua+Monte+Serrat,+1271&hl=pt-BR&ie=UTF8&ll=-23.548861,-46.559994&spn=0.008449,0.013937&sll=-23.548960,-46.560131&layer=c&cbp=13,337,,0,16.19&cbll=-23.548886,-46.560109&hnear=R.+Monte+Serrat,+1271+-+Tatuape,+S%C3%A3o+Paulo,+03312-001&t=m&z=17&panoid=9Mzj4rrX-ecMUVzSxkAz0A" target="_blank">(Saiba como chegar)</a>

    	<h2>ACLIMAÇÃO</h2>
    	Av. Lins de Vasconcelos, 1770 | Loja 6<br />
		<b>Fone: (11) 5083-7900 </b> <br />
		<a href="https://maps.google.com.br/maps?q=Av.+Lins+Vasconcelos,+1770&hl=pt-BR&ie=UTF8&ll=-23.578738,-46.624045&spn=0.008447,0.013937&sll=-23.578967,-46.624097&layer=c&cbp=13,7.4,,0,0&cbll=-23.578975,-46.624098&hnear=Av.+Lins+de+Vasconcelos,+1770+-+Vila+Mariana,+S%C3%A3o+Paulo,+01538-001&t=m&z=17&panoid=2McLR6OT519bz7vNam1bZw" target="_blank">(Saiba como chegar)</a>

    	<h2>JUNDIAÍ</h2>
    	Av. 9 de Julho, 1832 <br />(ao lado do Burguer King)<br />
		<b>Fone: (11) 4522-4141 </b> <br />
		<a href="https://maps.google.com.br/maps?q=Av.+Nove+de+Julho,+1832,+Jundia%C3%AD+-+S%C3%A3o+Paulo&hl=pt-BR&ie=UTF8&ll=-23.190192,-46.890217&spn=0.008471,0.013937&sll=-23.190079,-46.890134&layer=c&cbp=13,236.63,,0,12.54&cbll=-23.190211,-46.890299&hnear=Av.+Nove+de+Julho,+1832+-+Anhnagaba%C3%BA,+Jundia%C3%AD+-+S%C3%A3o+Paulo,+13208-056&t=m&z=17&panoid=5GsCJ8GOhfybunTcgTfh4A" target="_blank">(Saiba como chegar)</a>

    </div>
    <div style="text-align:center; margin-top:20px; float:left; width:100%">
    	<span style="font-family:Oswald; font-size:20px; color:#d4142b">HORÁRIO DE FUNCIONAMENTO</span>
    	<br />
    	<b>
    		Domingo a quinta-feira, das 18h às 23h30 <br />
			Sexta-feira e sábado, das 18h às 0h
		</b>
    </div>
</article>
<section class="imgEnderecos">
    <img src="imagens/detalhePizza.png" alt="" style="margin-top:255px" />
</section>
<div class="faixas"></div>


