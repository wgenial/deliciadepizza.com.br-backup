<div class="faixas"></div>
<article class="fundoEscuro">
    <h1> - Quem Somos</h1>
    <h6 class="textoDireita" style="margin-top:-60px">UM POUCO DA NOSSA HISTÓRIA</h6>
    <p style="margin-top:50px">
        A Delícia de Pizza foi criada para satisfazer a sua vontade de comer uma pizza quentinha e saborosa. Elaboramos as nossas pizzas com ingredientes de primeira qualidade, para garantir o resultado final ainda mais leve e crocante. Oferecemos uma ampla variedade de sabores, totalizando mais de 60 tipos de pizzas salgadas e doces que deixam qualquer um com água na boca.
    </p>
    <p>
        Nossa equipe de entrega é composta por profissionais selecionados, todos capacitados, experientes e comprometidos com a satisfação do cliente. Fazemos nossas entregas com rapidez e eficiência para garantir que a pizza chegue com o sabor impecável. Caso necessite, levamos até você equipamentos que possibilitam várias opções de pagamento, como VR Smart, Sodexho, além de cartão de crédito e débito automático, garantindo ainda mais praticidade no seu pedido.
    </p>
    <p>
        Nossos estabelecimentos estão divididos em seis endereços diferentes pela cidade e estado de São Paulo. Confira o mais próximo de você e bom apetite! 
    </p>
</article>
<section class="imgQuemSomos">
    <img src="imagens/detalhePizza.png" alt="" style="margin-top:255px" />
</section>
<div class="faixas"></div>


