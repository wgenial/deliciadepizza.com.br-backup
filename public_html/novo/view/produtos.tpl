<article class="colunaMaior esquerda">
    <h1>NOSSOS PRODUTOS <br /> <span>AS MELHORES MARCAS</span></h1>
    <br />
    <br />
    <h2 class="amarelo">&bull; PIZZAS</h2>
    <p>
        • Grande (8 pedaços)<br />
        • Broto (4 pedaços)<br />
        • Assados em forna a lenha (eucalípto)<br />
        • Usamos somente produtos de alta qualidade<br /><br />
    </p>
    <h2 class="amarelo">&bull; ESFIHAS</h2>
    <p>
        • Assados em forna a lenha (eucalípto).<br />
        • Usamos somente produtos de alta qualidade.<br /><br />
    </p>
    <h2 class="amarelo">&bull; BEBIDAS</h2>
    <p>
        • Refrigerantes descartáveis, 2 litros, 600ml e latas.<br />
        • Cervejas em lata<br />
        • Água mineral<br />
        • Chopp de vinho lata<br />
        • Suco lata e litro<br /><br />
    </p>
    <h2 class="amarelo">&bull; NOSSOS PARCEIROS</h2>
    <p>
        • Catupiry®<br />
        • Scala<br />
        • Sadia<br />
        • Coca-Cola<br />
        • Brahma<br /><br />
    </p>

</article>
<?php require("pedidos.tpl"); ?>
</div>
</section>

