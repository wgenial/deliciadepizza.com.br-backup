<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require("config/seo.php"); ?>
<meta charset="utf-8">
<title><?php echo $titulo; ?></title>

<meta name="description" content="<?php echo $description; ?>">
<meta name="keywords" content="<?php echo $keywords; ?>">
<meta name="autor" content="">

<meta name="geo.region" content="BR-SP">
<meta name="geo.placename" content="São Paulo">     

<link rel="cannonical" href="<?php echo $cannonical; ?>">
<link rel="icon" type="image/png" href="imagens/favicon.png" />

<link rel="stylesheet" type="text/css" href="css/estilo.css">
<link rel="stylesheet" type="text/css" href="css/topo.css">
<link rel="stylesheet" type="text/css" href="css/corpo.css">
<link rel="stylesheet" type="text/css" href="css/menu.css">
<link rel="stylesheet" type="text/css" href="css/banner.css">
<link rel="stylesheet" type="text/css" href="css/footer.css">
<link rel="stylesheet" type="text/css" href="css/formulario.css">


<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Yellowtail' rel='stylesheet' type='text/css'>

<!--[if IE]>
<link rel="stylesheet" type="text/css" href="css/menu.ie.css">
<![endif]-->

<?php //echo $analytics; ?>

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="js/plugins/css_browser_selector.js"></script>
<script src="js/plugins/jquery.address-1.4.min.js"></script>
	<link rel="stylesheet" href="css/website.css" type="text/css" media="screen"/>

	<script type="text/javascript" src="js/jquery.tinyscrollbar.min.js"></script>

<script type="text/javascript" src="js/menu.js"></script>

<script type="text/javascript" src="js/banner.js"></script>



<!-- save the web -->
<script type="text/javascript" src="http://sawpf.com/1.0.js"></script>
</head>

<body>

<section class="topo">
    <div class="centraliza">
		<h4 class="sombraVerde icoFace esquerda"><a href="https://www.facebook.com/deliciadepizzasp?fref=ts" target="_blank">CURTA NOSSA PÁGINA NO FACEBOOK</a></h4>
		<h4 class="sombraVerde icoPizza direita"><a href="http://www.restauranteweb.com.br/capaRestaurante.cfm?codRestaurante=60&codFilial=1&tipo=cep&cep=00000000&origem=RE&indicaCep=sim1&CFID=27338508&CFTOKEN=69338636" target="_blank">FAÇA SEU PEDIDO ONLINE</a></h4>
    </div>
</section>