<div class="faixas"></div>

<article class="fundoEscuro">

    <h1>Cadastro</h1>
    <h6 class="textoDireita" style="margin-top:-60px">Novidades e promoções</h6>
    <?php 
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        require_once("controller/cadastro.php");
    }
    ?>
    
    <form name="cadastro" id="cadastro" method="post">
        <ul>
            <li>
                <div class="textoForm">
                    <label for="nome">Nome:</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="text" name="nome" id="nome" required />
                </div>
            </li>
            <li>
                <div class="textoForm">
                    <label for="email">E-mail:</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="email" name="email" id="email" required />
                </div>
            </li>
            <li>
                <div class="textoForm">
                    <label for="endereco">Endereço:</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="text" name="endereco" id="endereco" required />
                </div>
            </li>
            <li>
                <div class="textoForm">
                    <label for="numero">Número</label>
                </div>
                <div class="campoPequenoForm">
                    <input type="text" name="numero" id="numero" required />
                </div>
                <div class="textoPequenoForm">
                    <label for="cep">
                            CEP: 
                            <!--
                            <a href="http://www.buscacep.correios.com.br/servicos/dnec/menuAction.do?Metodo=menuEndereco" target="_blank" style="font-size:12px; font-weight:normal; color:#e2162e">
                                (consultar)
                            </a>:
                            -->
                    </label>
                </div>
                <div class="campoMedioForm">
                    <input type="text" name="cep" id="cep" required />
                </div>
            </li>
            <li>
                <div class="textoForm">
                    <label for="bairro">Bairro:</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="text" name="bairro" id="bairro" required />
                </div>
            </li>
            <li>
                <div class="textoForm">
                    <label for="bairro">Complemento:</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="text" name="complemento" id="complemento" />
                </div>
            </li>
            <li>
                <div class="textoForm">
                    <label for="cidade">Cidade:</label>
                </div>
                <div class="campoMedioForm">
                    <input type="text" name="cidade" id="cidade" required />
                </div>
                <div class="textoPequenoForm">
                    <label for="uf">UF:</label>
                </div>
                <div class="campoPequenoForm">
                    <input type="text" name="uf" id="uf" required />
                </div>
            </li>
            <li>
                <input type="submit" value="enviar" class="botao amarelo direita" value="ENVIAR" />
            </li>
        </ul>
    </form>
</article>
<section class="imgCadastro">
    <img src="imagens/detalhePizza.png" alt="" style="margin-top:255px" />
</section>
<div class="faixas"></div>

