<?php 
require("controller/menuCategoriasProdutos.php");
require("controller/produtos.php"); 
?>

<div class="faixas"></div>

<article class="fundoEscuro">

    <h1 id="ancora1">
        Cardápio
    </h1>
    <br />
    <br />

        <div id="scrollbar1">
            <div class="scrollbar">
                <div class="track">
                    <div class="thumb"><div class="end"></div>
                </div>
            </div>
        </div>
        <div class="viewport">
            <section class="menuLateral">
                <div class="boxAmarelo fundoPizza">
                    <div class="tituloBoxAmarelo">
                        <a href="cardapio-1">PIZZAS</a>
                    </div>
                </div>
                <div class="boxAmarelo fundoSobremesa">
                    <div class="tituloBoxAmarelo">
                        <a href="cardapio-2">PIZZAS ESPECIAIS E DOCES</a>
                    </div>
                </div>
                <div class="boxAmarelo fundoBebida">
                    <div class="tituloBoxAmarelo">
                        <a href="?rota=view/cardapio&amp;id=3">BEBIDAS E SOBREMESAS</a>
                    </div>
                </div>
            </section>
            <div class="overview">
                <?php
                echo $produto->erro;
                if($produto->total >= 0){
                    for ($i=0; $i<=$produto->total; $i++) {
                ?>
                <p class="esquerda" style="clear:left; line-height:16px">
                    <span class="vermelho pizza"><?php echo $produto->titulo[$i]; ?></span> 
                    <br />
                    <i><?php echo strip_tags($produto->texto[$i]); ?></i>
                </p>
                <?php
                    }
                }
                ?>
            </div>
        </div>
</div>
</article>

<section class="imgCardapio">
    <img src="imagens/detalhePizza.png" alt="" style="margin-top:255px" />
</section>
<div class="faixas"></div>
<script>
$(document).ready(function($) {
     $('#scrollbar1').tinyscrollbar();  
});
</script>


