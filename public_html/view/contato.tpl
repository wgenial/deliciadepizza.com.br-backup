<div class="faixas"></div>
<article class="fundoEscuro">

    <h1>Fale Conosco</h1>
    <h6 class="textoDireita" style="margin-top:-60px; margin-bottom: 1em;">
        Sua opinião é muito importante!
        <br />
        <a href="trabalhe" style="font-size:12px; color: #dd152d" class="link">Trabalhe Conosco, clique aqui.</a>
    </h6>

    <?php 

    if($_SERVER['REQUEST_METHOD'] == 'POST' and $_POST["nome"] != "" and $_POST["email"] != "" and $_POST["assunto"] != ""  and $_POST["mensagem"] != ""){
        require_once("controller/contato.php");
    }

    ?>

    <form name="contato" id="contato" method="post">
        <ul>
            <li>
                <div class="textoForm">
                    <label for="nome">Nome:</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="text" name="nome" id="nome" required />
                </div>
            </li>
            <li>
                <div class="textoForm">
                    <label for="email">E-mail:</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="email" name="email" id="email" required />
                </div>
            </li>
            <li>
                <div class="textoForm">
                    <label for="assunto">Assunto:</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="text" name="assunto" id="assunto" required />
                </div>
            </li>
            <li>
                <div class="textoForm">
                    <label for="aniversario">Aniversário:</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="text" name="aniversario" id="aniversario" required />
                </div>
            </li>
            <li>
                <div class="textoForm">
                    <label for="assunto">Telefone:</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="text" name="telefone" id="telefone" required />
                </div>
            </li>
            <li style="height:auto;">
                <div class="textoForm">
                    <label for="mensagem">Mensagem:</label>
                </div>
                <div class="campoGrandeForm">
                    <textarea name="mensagem" id="mensagem" rows="4"></textarea>
                </div>
            </li>
            <li>
                <input type="submit" value="enviar" class="botao amarelo direita" />
            </li>
        </ul>
    </form>
</article>

<section class="imgEnderecos">
    <img src="imagens/detalhePizza.png" alt="" style="margin-top:255px" />
</section>

<div class="faixas"></div>