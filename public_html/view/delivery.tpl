<div class="faixas"></div>

<article class="fundoEscuro">
    <h1> - Delivery</h1>
    <h6 class="textoDireita" style="margin-top:-60px">maior comodidade</h6>
    <p>
    	<img src="imagens/delicia_delivery.jpg" alt="" title="" class="esquerda" style="margin-right:20px;" />
        Nossas entregas são feitas pela equipe delivery, composta de profissionais selecionados, dotados de capacitação, experiência e responsabilidade, comprometidos com a satisfação do cliente.
    <br />
    <br />
        Aqui na Delícia de Pizza, o exigente consumidor paulistano recebe um atendimento de alta qualidade, que supera as suas expectativas. 
    <br />
    <br />
		Para facilitar e trazer maior comodidade aos nossos clientes, os entregadores podem levar consigo equipamentos que possibilitam várias opções de pagamento, como VR Smart, Sodexho, além de cartão de crédito e débito automático.        
    </p>
    <div class="linksDelivery">
    	<a href="" onclick="mudarEndereco ( 'enderecos' )">CONHEÇA NOSSAS UNIDADES</a>

	    <h4 style="background-color: rgb(253, 184, 19);padding: 0.1em 0.5em;background-position: 11px 4px;background-size: 25px 25px;float: none;max-width: 230px;margin: 1em auto;" class="sombraVerde icoPizza direita"><a href="https://papdelivery.com.br?profile_id=9212" target="_blank" style="color: rgb(255, 255, 255);">FAÇA SEU PEDIDO ONLINE</a></h4>

		<!-- &nbsp; &nbsp;<a href="">|</a> &nbsp; &nbsp;<a href="http://www.restauranteweb.com.br/capaRestaurante.cfm?codRestaurante=60&amp;codFilial=1&amp;tipo=cep&amp;cep=00000000&amp;origem=RE&amp;indicaCep=sim1&amp;CFID=27338508&amp;CFTOKEN=69338636" target="_blank">FAÇA SEU PEDIDO ONLINE</a>-->
    </div>
</article>

<section class="imgDelivery">
    <img src="imagens/detalhePizza.png" alt="" style="margin-top:255px" />
</section>

<div class="faixas"></div>


