<div class="faixas"></div>
<article class="fundoEscuro">
    <h1> - Endereços</h1>
    <h6 class="textoDireita" style="margin-top:-60px">escolha o mais perto de você</h6>
    <div class="meia">

    	<h2 style="margin-top:-20px">VILA LEOPOLDINA</h2>
    	Rua Schiling, 300 <br />
		<b>Fone: (11) 3641-6778 </b> <br />
		<a href="https://maps.google.com.br/maps?q=Rua+Schilling,+300,+S%C3%A3o+Paulo&amp;hl=pt-BR&amp;ie=UTF8&amp;sll=-23.201096,-46.908366&amp;sspn=0.542128,0.891953&amp;oq=Rua+Schiling,+300+&amp;hnear=R.+Schilling,+300+-+Vila+Leopoldina,+S%C3%A3o+Paulo,+05302-000&amp;t=m&amp;z=17&amp;iwloc=A" target="_blank">(Saiba como chegar)</a>

    	<h2>PANAMBY OPEN MALL</h2>
    	R. Antônio da C. Barbosa, s/n°<br />
		<b>Fone: (11) 3776-7776 </b> <br />
		<a href="https://maps.google.com.br/maps?q=Del%C3%ADcia+de+Pizza+Panamby,+Rua+Ant%C3%B4nio+da+Costa+Barbosa+-+Vila+Andrade,+S%C3%A3o+Paulo,+05717-220&amp;hl=pt-BR&amp;ie=UTF8&amp;sll=-23.609138,-46.687689&amp;sspn=0.067557,0.111494&amp;hq=Del%C3%ADcia+de+Pizza&amp;hnear=R.+Ant%C3%B4nio+da+Costa+Barbosa+-+Vila+Andrade,+S%C3%A3o+Paulo,+05717-220&amp;t=m&amp;z=17" target="_blank">(Saiba como chegar)</a>

    </div>
    <div class="meia" style="margin-left:20px">
        <!--
        <h2 style="margin-top:-20px">ACLIMAÇÃO</h2>
        Av. Lins de Vasconcelos, 1770 <br />
        <b>Fone: (11) 5083-7900 </b> <br />
        <a href="https://maps.google.com.br/maps?q=Av.+Lins+de+Vasconcelos,+1170+aclima%C3%A7%C3%A3o&hl=pt-BR&ie=UTF8&ll=-23.573732,-46.622994&spn=0.011633,0.019945&sll=-23.573745,-46.623080&hnear=Av.+Lins+de+Vasconcelos,+1170+-+Cambuci,+S%C3%A3o+Paulo,+01538-000&t=m&z=17" target="_blank">(Saiba como chegar)</a>


    	<h2>JUNDIAÍ</h2>
    	Av. 9 de Julho, 1832 <br />(ao lado do Burger King)<br />
		<b>Fone: (11) 4522-4141 </b> <br />
		<a href="https://maps.google.com.br/maps?q=Av.+Nove+de+Julho,+1832,+Jundia%C3%AD+-+S%C3%A3o+Paulo&amp;hl=pt-BR&amp;ie=UTF8&amp;ll=-23.190192,-46.890217&amp;spn=0.008471,0.013937&amp;sll=-23.190079,-46.890134&amp;layer=c&amp;cbp=13,236.63,,0,12.54&amp;cbll=-23.190211,-46.890299&amp;hnear=Av.+Nove+de+Julho,+1832+-+Anhnagaba%C3%BA,+Jundia%C3%AD+-+S%C3%A3o+Paulo,+13208-056&amp;t=m&amp;z=17&amp;panoid=5GsCJ8GOhfybunTcgTfh4A" target="_blank">(Saiba como chegar)</a>
        -->
    </div>
    <div style="text-align:center; margin-top:20px; float:left; width:100%">
    	<span style="font-family:Oswald; font-size:20px; color:#d4142b">HORÁRIO DE FUNCIONAMENTO</span>
    	<br />
    	<b>
    		Domingo a quinta-feira, das 18h às 23h30 <br />
			Sexta-feira e sábado, das 18h às 0h
		</b>
    </div>
</article>
<section class="imgEnderecos">
    <img src="imagens/detalhePizza.png" alt="" style="margin-top:255px" />
</section>
<div class="faixas"></div>


