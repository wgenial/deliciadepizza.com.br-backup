<?php

function carregaPagina ( $rota ){
	if ( testaGet ( $rota ) ) {
		include("view/".$_GET[$rota]);
	}else {
		include('home.tpl');
	}
}

function testaGet ( $get ) {
	if (isset($_GET[$get])) {
		echo "view/".$_GET[$get];
	    if(file_exists($_GET[$get])){
	        return true ;
	    }else{
	        return false;
	    }
	} else {
	    return false;
	}
}

carregaPagina ( 'rota' );
?>