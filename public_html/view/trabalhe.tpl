<div class="faixas"></div>
<article class="fundoEscuro">
    <h1>Trabalhe Conosco</h1>
    <?php 
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        require_once("controller/trabalhe.php");
    }
    ?>
    <form name="contato" id="contato" method="post" enctype="multipart/form-data">
        <ul>
            <li>
                <div class="textoForm">
                    <label for="nome">Nome:</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="text" name="nome" id="nome" required />
                </div>
            </li>
            <li>
                <div class="textoForm">
                    <label for="email">E-mail:</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="email" name="email" id="email" required />
                </div>
            </li>
            <li>
                <div class="textoForm">
                    <label for="telefone">Telefone:</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="text" name="telefone" id="telefone" required />
                </div>
            </li>
            <li style="height:auto;">
                <div class="textoForm">
                    <label for="mensagem">Mensagem:</label>
                </div>
                <div class="campoGrandeForm">
                    <textarea name="mensagem" id="mensagem" rows="4"></textarea>
                </div>
            </li>
            <li>
                <div class="textoForm">
                    <label for="assunto">Currículo (.doc):</label>
                </div>
                <div class="campoGrandeForm">
                    <input type="file" name="anexo" id="anexo" required />
                </div>
            </li>
            <li>
                <input type="submit" value="enviar" class="botao amarelo direita" />
            </li>
        </ul>
    </form>
</article>
<section class="imgEnderecos">
    <img src="imagens/detalhePizza.png" alt="" style="margin-top:255px" />
</section>
<div class="faixas"></div>